package com.ajdevmobile.conversormoeda

import android.opengl.Visibility
import com.ajdevmobile.conversormoeda.R


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.TextView
import org.json.JSONObject
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class MainActivity : AppCompatActivity() {

    var bt_converter : Button? = null
    var txt_moeda : TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        txt_moeda = findViewById(R.id.txt_moeda)

        bt_converter = findViewById(R.id.bt_converter)
        bt_converter?.setOnClickListener {

              converter_valor()

        }


    }


    fun converter_valor(){


        var group_radio = findViewById<RadioGroup>(R.id.rd_group_moeda)

        var moeda =  group_radio.checkedRadioButtonId

        var currency = when(moeda){

               R.id.rd_usd ->{
                   "USD_BRL"
               }

              R.id.rd_euro ->{
                  "EUR_BRL"
              }

              else -> {
                  "CLP_BRL"
              }

        }


        var ed_valor = findViewById<EditText>(R.id.editTextNumberDecimal)

        var valorRecebido = ed_valor.text.toString()

        if(valorRecebido.isEmpty())
            return

        Thread{

            var url = URL("https://free.currconv.com/api/v7/convert?q=${currency}&compact=ultra&apiKey=537fc2474e12bd3c05db")

            var con = url.openConnection() as HttpsURLConnection

            var data = con.inputStream.bufferedReader().readText()


            runOnUiThread {


                var jsonObject = JSONObject(data)

                var valorRetornadoApi = jsonObject.getDouble("${currency}")

                Log.i("MOEDA", "converter_valor: " +valorRetornadoApi.toString() )

                txt_moeda?.text =    "%.2f".format((valorRecebido.toDouble()) * valorRetornadoApi)
                txt_moeda!!.visibility = View.VISIBLE

            }




        }.start()







    }
}